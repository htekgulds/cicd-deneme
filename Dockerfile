FROM node:alpine

RUN npm install -g http-server
COPY . .

ENTRYPOINT [ "http-server" ]